<?php

namespace ImaginationMedia\Correios\Model\ResourceModel\Cotacoes;

use Magento\Framework\Data\Collection\Db\FetchStrategyInterface as FetchStrategy;
use Magento\Framework\Data\Collection\EntityFactoryInterface;
use Magento\Framework\Event\ManagerInterface as EventManager;
use Magento\Framework\View\Element\UiComponent\DataProvider\SearchResult;
use ImaginationMedia\Correios\Model\ResourceModel\Cotacoes;
use Psr\Log\LoggerInterface as Logger;

class CollectionDataProvider extends SearchResult
{
    public function __construct(
        EntityFactoryInterface $entityFactory,
        Logger                 $logger,
        FetchStrategy          $fetchStrategy,
        EventManager           $eventManager,
                               $mainTable = 'correios_cotacoes',
                               $resourceModel = Cotacoes::class,
                               $identifierName = null,
                               $connectionName = null
    ) {
        parent::__construct(
            $entityFactory,
            $logger,
            $fetchStrategy,
            $eventManager,
            $mainTable ?? 'correios_cotacoes',
            $resourceModel ?? Cotacoes::class,
            $identifierName,
            $connectionName
        );
    }
}